//-------Задание №1 Среднее геометрическое

function getRandomNumber(min, max){
    let random = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(random); 
}

let randomArray;

function getRandomArray(){
    let Array = [];
    for(let i = 0; i < 10; i++){
    let number = getRandomNumber(1, 100);
    Array.push(number);
    } 
    return  Array;
}

randomArray = getRandomArray();

console.log(randomArray);

function getGeometric(Array){
    let pr = 1;
    Array.forEach(item => {
        pr *= item;
    });

    let Geometric = Math.pow(pr, 1/Array.length);
    return Geometric;
}

let Geometric = getGeometric(randomArray);
console.log(Geometric);


// Задание №2 - добавление уникального ID


const NAMES = ['Олег', 'Павел', 'Костя', 'Толя', 'Андрей', 'Петр', 'Cлава', 'Александр', 'Вася', 'Сергей'];
let users;

function generateRandomUsers() {
    
    let array = [];
    for(let i = 0 ; i < 5; i++){
        let user = {
            name: NAMES[getRandomNumber(0, NAMES.length - 1)],
            age: getRandomNumber(10,100),
            id: findMaxID(array) + 1
        };
        array.push(user);
    }
    return array;
}

function findMaxID(array) {
    let max = array.reduce((accum, item) =>{
    if (item.id > accum){
        accum = item.id;
    } 
    return accum;
    }, 0); 
    return max;
}

users = generateRandomUsers();

console.log(users);

// Задание 3 - поиск процента пользователей старше 50 лет

let percentUsersOverAge50;

function getUsersOverAge50(array){
    let overAge50 = array.filter(item => item.age > 50);
    let percentUsersOverAge50 = (overAge50.length * 100)/array.length;
    return percentUsersOverAge50;
}

percentUsersOverAge50 = getUsersOverAge50(users);

console.log(percentUsersOverAge50);

// Задание 4 - сортировка пользователей по имени (по возрастанию)

function sortByName(array){
    array.sort((a, b) => (a.name).localeCompare(b.name));
}

sortByName(users);

console.log(users);

// Задание 5 - получение массива имен

let arrayName = users.map(item => item.name);

console.log(arrayName);

// Задание 6 - удаление пользователя по ID

let newUsersDelete = [];

function deleteUserByID(array){
    let deleteID = Number(prompt('Введите ID для удаления'));
    let i = 0;

    array.forEach(item => {
        if(item.id === deleteID){
            i++  
        }
    });

    if(i === 0){
        alert('Совпадений не найдено!');
    }
    else{
        let newArray = array.filter(item => item.id !== deleteID);
        return  newArray;
    }
}

newUsersDelete = deleteUserByID(users);

if(newUsersDelete !== undefined){
    console.log(newUsersDelete);
}

// Задание 7 - добавление пользователя в конец

let newUsersAdd = [];

function addUser(array){
        user = {
        name: NAMES[getRandomNumber(0, NAMES.length - 1)],
        age: getRandomNumber(10,100),
        id: findMaxID(array) + 1
    };
    array.push(user);
    return array;
}

newUsersAdd = addUser(users);

console.log(newUsersAdd);
